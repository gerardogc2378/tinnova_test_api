class CreateBeers < ActiveRecord::Migration[6.1]
  def change
    create_table :beers do |t|
      t.string :name
      t.string :tagline
      t.string :description
      t.float :abv

      t.timestamps
    end
  end
end
