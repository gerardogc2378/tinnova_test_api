class AddSeenAtToBeers < ActiveRecord::Migration[6.1]
  def change
    add_column :beers, :seen_at, :datetime
  end
end
