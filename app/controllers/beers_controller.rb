class BeersController < ApplicationController
  before_action :authenticate!

  def index
    render json: {
        hello: @current_user.name
    }
  end

  def save_my_favorite
    beer = Beer.find(params[:beer_id])
    if beer
      favorite_beer = FavoriteBeer.find_or_create_by(user_id: @current_user.id, beer_id: beer.id)
      render json: { favorite_beer: favorite_beer }
    else
      render json: { favorite_beer: nil }
    end
  end

  def get_my_favorite
    # beer_list = Beer.joins(:favorite_beers).where("favorite_beers.user_id = ?", @current_user.id)
    beer_list = Beer.my_favorite(@current_user.id)
    render json: { favorite_beer: beer_list }
  end

  def get_all
    beer_list = Beer.get_all(@current_user.id)
    render json: { beer_list: beer_list }
  end

  def punk_beers
    if Beer.count == 0
      faraday_handler.each do |item|
        Beer.find_or_create_by(name: item["name"], tagline: item["tagline"], description: item["description"], abv: item["abv"])
      end
    end
    if params[:name] or params[:abv]
      if params[:name]
        beer_list = Beer.where("name like ?", "%#{params[:name]}%")
      end
      if params[:abv]
        beer_list = Beer.where("abv = ?", params[:abv])
      end
      beer_list.each do |item|
        item.update(seen_at: Time.now)
      end
    else
      Beer.update_all(seen_at: Time.now)
      beer_list = Beer.all
    end
    render json: { beer_list: beer_list }
  end

  private

  def faraday_handler
    response = Faraday.get 'https://api.punkapi.com/v2/beers'
    JSON.parse(response.body)
  end
end
