class Beer < ApplicationRecord
  has_many :favorite_beers

  def self.get_all(id = 0)
    sql = "SELECT beers.*, false AS favorite FROM beers INNER JOIN favorite_beers ON favorite_beers.beer_id <> beers.id WHERE favorite_beers.user_id = #{id} UNION ALL SELECT beers.*, true AS favorite FROM beers INNER JOIN favorite_beers ON favorite_beers.beer_id == beers.id WHERE favorite_beers.user_id = #{id}"
    ActiveRecord::Base.connection.execute(sql)
  end

  def self.my_favorite(id = 0)
    self.joins(:favorite_beers).where("favorite_beers.user_id = ?", id)
  end
end
